require! sequelize:type

module.exports =
  'roomAccess': type.ENUM \denied \read \modify
  'fileAccess': type.ENUM \denied \read \modify
  'pageAccess': type.ENUM \denied \read \modify