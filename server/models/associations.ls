module.exports = ({account, activity, file, image, key, page, profile, room})!->
  account.has-one  profile,  'foreignKey':'owner'
  account.has-many activity, 'foreignKey':'owner'
  account.has-many key,      'foreignKey':'owner'

  account.has-many room, 'through':'favorites' 'as':'favorites' 'foreignKey':'room'
  room.has-many account, 'through':'favorites' 'foreignKey':'account'

  room.has-many key,  'foreignKey':'room'
  room.has-many room, 'foreignKey':'parent'
  room.has-many file, 'foreignKey':'room'
  room.has-many page, 'foreignKey':'room'