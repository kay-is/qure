require! sequelize:type

module.exports =
  'fullName':type.STRING
  'email':type.STRING
  'website':type.STRING
  'description':type.TEXT
  'image':type.BLOB
  'mimetype':type.STRING