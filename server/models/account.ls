require! {
  crypto
  sequelize:type
}

module.exports =
  'username':
    'type': type.STRING
    'len': [3 100]
    'unique': yes
  'email':
    'type': type.STRING
    'allowNull': no
    'unique': yes
    'isEmail': yes
  'passwordHash':
    'type': type.STRING
    'allowNull': no
    'set':->
      hash = crypto
      .create-hash \sha256
      .update \qureSalt#
      .update it
      .digest \hex

      @set-data-value \passwordHash hash
  'isAdmin':
    'type': type.BOOLEAN
    'defaultValue': no