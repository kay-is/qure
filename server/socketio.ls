require! {
  '../config':{web}
  'socket.io':socket-io
  'socketio-jwt':jwt
  './helpers/logger'
}
log = logger \Socket.IO \green
module.exports = (server)->
  log 'Initialize...'
  io = socket-io server

  io.set \authorization jwt.authorize do
    'secret':web.secret
    'handshake':yes

  io.on \connection (client)!->
    log 'Client connected:' client.conn.id
    client.on \disconnect !->
      log 'Client disconnected:' client.conn.id

  log 'Initialized!'
  return io