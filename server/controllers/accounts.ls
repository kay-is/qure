module.exports = (router, {account})!->

  router.get '/' (request, send)!->
    all-accounts <-! account
      .find-all 'attributes':<[id username]>
      .success
    send.json 'accounts':all-accounts