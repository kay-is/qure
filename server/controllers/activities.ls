require! '../helpers/logger'
log = logger \Activities \red

module.exports = (router, {activity, account})!->

  router.post '' ({account-id, username, body:{type, name, parameter=''}}, send)!->
    activity <-! activity
      .create do
        'type':type
        'name':name
        'parameter':parameter
        'owner':account-id
      .success
    log username, 'did ' + type + ':' +  name + '(' + parameter + ')'
    send.json {activity}

  router.get '' ({account-id, username}, send)!->
    activities <-! activity
      .find-all do
        'where':'owner':account-id
      .success
    send.json {activities}

    list = []
    for activity in activities
      list.push activity.name
    log 'Delivered', username, 'these activities:' list.join ', '