module.exports = (router, {account, key, room})!->

  router.post '/' ({account-id, body}, send)!->
    room-id = body.key.room
    modify-room-key <-! key
      .find 'where':
        'room':room-id
        'owner':account-id
        'roomAccess':\modify
      .success

    unless modify-room-key
      return send.unauthorized 'No permission to create key for room ' + room-id

    key-definition =
      'room':parse-int room-id
      'roomAccess':body.key.room-access
      'pageAccess':body.key.page-access
      'fileAccess':body.key.file-access

    created-key <-! key
      .create key-definition
      .success

    send.status 201 .json 'key':created-key

  router.put '/:keyId' ({account-id, body, params}, send)!->
    room-id = body.key.room
    modify-room-key <-! key
      .find 'where':
        'room':room-id
        'owner':account-id
        'roomAccess':\modify
      .success

    unless modify-room-key
      return send.unauthorized 'No permission to edit keys of room ' + room-id

    key-to-update <-! key
      .find params.key-id
      .success

    unless key-to-update
      return send.not-found 'Key ' + params.key-id + ' does not exist!'

    update = {}
    update.room-access = body.key.room-access if body.key.room-access
    update.page-access = body.key.page-access if body.key.page-access
    update.file-access = body.key.file-access if body.key.file-access
    update.owner = parse-int body.key.owner if body.key.owner

    updated-key <-! key-to-update
      .update-attributes update
      .success

    send.json 'key':updated-key

  router.delete '/:keyId' ({account-id, body, params}, send)!->
    key-to-delete <-! key
      .find params.key-id
      .success

    room-id = key-to-delete.room

    modify-room-key <-! key
      .find 'where':
        'owner':account-id
        'room':room-id
        'roomAccess':\modify
      .success

    unless modify-room-key
      return send.unauthorized 'No permission to delete keys in room ' + room-id

    unless key-to-delete
      return send.status 204nocontent .end!

    <-! key-to-delete
      .destroy!
      .success

    send.status 204nocontent .end!

  router.get '/' ({account-id, query}, send)!->
    unless query.room or query.owner
      return send.bad-request 'No room or owner specified in query!'

    database-query = 'where':{}
    database-query.room = query.room if query.room
    database-query.owner = query.owner if query.owner

    keys <-! key
      .find-all database-query
      .success

    account-ids = []
    for found-key in keys
      account-ids.push found-key.owner

    accounts <-! account
      .find-all do
        'where':'id':account-ids
        'attributes':<[id username]>
      .success

    send.json {keys, accounts}