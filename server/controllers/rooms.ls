module.exports = (router, {room, key, page}, io)!->

  router.get '/' ({account-id, query}, send)!->
    if query.parent
      load-children account-id, query.parent, send
    else
      load-all account-id, send

    !function load-all(account-id, send)
      all-user-keys <-! key
        .find-all 'where':'owner':account-id
        .success

      # only rooms the user has a key for
      available-room-ids = []
      keys-for-delivery = []
      for key in all-user-keys
        continue if key.room-access is \denied
        available-room-ids.push key.room
        keys-for-delivery.push key

      rooms-for-delivery <-! room
        .find-all 'where':'id':available-room-ids
        .success

      send.json 'rooms':rooms-for-delivery, 'keys':keys-for-delivery

    !function load-children(account-id, parent, send)
      all-user-keys <-! key
        .find-all 'where':'owner':account-id
        .success

      available-room-ids = []
      for key in all-user-keys
        continue if key.room-access is \denied
        available-room-ids.push key.room

      rooms-for-delivery <-! room
        .find-all 'where':
          'id':available-room-ids
          'parent':parent
        .success

      keys-for-delivery = []
      for user-key in all-user-keys
        for child-room in rooms-for-delivery
          if user-key.room is child-room.id
            keys-for-delivery.push user-key
            break

      send.json 'rooms':rooms-for-delivery, 'keys':keys-for-delivery

  router.get '/:id' ({account-id, params:{id}}, send)!->
    id = parse-int id
    found-key <-! key
      .find 'where':
        'owner':account-id
        'room':id
      .success

    if !key or key.room-access is \denied
      return send.unauthorized!

    found-room <-! room
      .find id
      .success

    unless found-room
      return send.not-found 'Room does not exist!'

    send.json 'room':found-room, 'keys':[found-key]

  router.delete '/:id' ({params:{id}, body, username, account-id}, send)!->
    id = parse-int id
    found-key <-! key
      .find 'where':
        'owner':account-id
        'room':id
        'roomAccess':\modify
      .success

    unless found-key
      return send.unauthorized 'No permission to delete room with ID: ' + id

    <-! key
      .destroy 'where':'room':id
      .success

    <-! page
      .destroy 'where':'room':id
      .success

    <-! room
      .destroy 'where':'id':id
      .success

    send.status 204nodata .end!
    io.emit \dataDelete 'room':id, 'username':username

  router.put '/:id' ({params:{id}, body, username, account-id}, send)!->
    found-key <-! key
      .find 'where':
        'owner':account-id
        'room':id
      .success

    if !key or key.room-access isnt \modify
      return send.unauthorized!

    room-to-update <-! room
      .find id
      .success

    unless room-to-update
      return send.not-found 'Room does not exist!'

    updated-room <-! room-to-update
      .update-attributes body.room
      .success

    send.json 'room':updated-room
    io.emit \dataUpdate 'room':updated-room, 'username':username

  router.post '/' ({account-id, body:{room:{title, description, parent}}}, send)!->
    parent = parse-int parent
    parent-room-key <-! key
      .find 'where':
        'owner':account-id
        'room':parent
      .success

    if !parent-room-key or parent-room-key.room-access isnt \modify
      return send.unauthorized!

    desired-parent-room <-! room
      .find parent
      .success

    unless desired-parent-room
      return send.not-found 'Parent room does not exist!'

    new-room <-! room
      .create {title, description, parent}
      .success
    owner-key <-! key
      .create do
        'owner':account-id
        'room':new-room.id
        'roomAccess':\modify
        'pageAccess':\modify
        'fileAccess':\modify
      .success
    first-page <-! page
      .create do
        'room':new-room.id
        'title':'First Page'
        'text':'Welcome to room *' + title + '*'
      .success

    new-room.first-page = first-page.id
    new-room.save!

    send.json 'room':new-room, 'keys':[owner-key], 'pages':[first-page]