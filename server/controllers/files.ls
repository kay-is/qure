module.exports = (router, {key, file})!->

  router.get '/:fileId/download' ({account-id, params}, send)!->
    found-file <-! file
      .find params.file-id
      .success

    if !found-file
      return send.not-found 'File ' + params.file-id + ' does not exist!'

    send.set \Content-Type found-file.mime-type
    send.set \Content-Disposition 'attachment; filename=' found-file.name + \;

    send.end found-file.content

  router.post '/' ({account-id, body}, send)!->

    unless body.file
      return send.bad-request 'Invalid request. File object in JSON expected!'

    new-file = body.file

    modify-files-key <-! key
      .find 'where':
        'room':new-file.room
        'owner':account-id
        'fileAccess':\modify
      .success

    unless modify-files-key
      return send.unauthorized 'No permission to upload file to room ' + new-file.room

    try
      content = new Buffer new-file.content, \base64
    catch
      return send.bad-request 'Invalid file content. Base64 string expected!'

    created-file <-! file
      .create do
        'name':new-file.name
        'room':new-file.room
        'content':content
        'mimeType':new-file.mime-type
      .success

    send.json 'file':
      'id':created-file.id
      'name':created-file.name
      'room':created-file.room