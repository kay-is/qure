require! {
  crypto
  'jsonwebtoken':jwt
  '../../config':web:{secret}
  '../helpers/logger'
}

log = logger \Auth \yellow

ERROR_MSG_TOKEN = 'Wrong username or password!'
ERROR_MSG_SIGNUP = 'Username or email already exist!'

module.exports = (router, {account, profile, key})!->

  router.post '/token' (body:{username, password}, send)!->
    account-found <-! account
      .find 'where':'username':username
      .success

    if !account-found or account-found.passwordHash isnt hashed password
      log 'Token denied for' username
      return send.bad-request ERROR_MSG_TOKEN

    send.json do
      'token':token-for do
        'isAdmin':account-found.is-admin
        'accountId':account-found.id
        'username':account-found.username
      'account':
        'id':account-found.id
        'username':account-found.username
        'isAdmin':account-found.is-admin

    log 'Token delivered for' username

  router.post '/signup' (body:{username, password, email},send)!->
    account-found <-! account
      .find 'where':'username':username
      .success

    if account-found
      return send.bad-request ERROR_MSG_SIGNUP

    created-acccount <-! account
      .create do
        'username':username
        'passwordHash':password
        'email':email
      .success

    <-! profile
      .create 'owner':created-acccount.id
      .success

    <-! key
      .create do
        'room':1
        'owner':created-acccount.id
        'roomAccess':\read
        'pageAccess':\read
        'fileAccess':\read
      .success

    token-payload =
      'isAdmin':no
      'accountId':created-acccount.id
      'username':created-acccount.username

    send.json do
      'token':token-for token-payload
      'account':
        'id':created-acccount.id
        'username':created-acccount.username
        'isAdmin':no

function hashed
  crypto
  .create-hash \sha256
  .update \qureSalt#
  .update it
  .digest \hex

function token-for
  jwt.sign it, secret