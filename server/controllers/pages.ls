module.exports = (router, {page, room}, io)!->

  router.get '/' (request, send)!->
    unless request.query.room
      return send.bad-request 'Parameter room needed!'

    all-pages <-! page
      .find-all 'where':'room':request.query.room
      .success
    send.json 'pages':all-pages

  router.get '/:id' (params:{id}, send)!->
    found-page <-! page
      .find id
      .success
    send.json 'page':found-page

  router.put '/:id' ({account-id, is-admin, body, params}, send)!->
    page-to-update <-! page
      .find params.id
      .success

    unless page-to-update
      return send.not-found 'Page does not exist!'

    updated-page <-! page-to-update
      .update-attributes body.page
      .success

    send.json 'page':updated-page
    io.emit \dataUpdate 'page':updated-page

  router.post '/' ({socket, body}, send)!->
    page-definition = body.page
    new-page <-! page
      .create do
        title:page-definition.title
        text:page-definition.text
        room:parse-int page-definition.room
      .success
    send.json 'page':new-page