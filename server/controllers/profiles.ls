module.exports = (router, {account, profile}, io)!->

  visible-attributes =
    * \id
    * \fullName
    * \email
    * \website
    * \description
    * \createdAt

  router.get '' (request, send)!->
    profiles <-! profile
      .find-all 'attributes':visible-attributes
      .success

    send.json {profiles}

  router.get '/:id' ({params}, send)!->
    found-profile <-! profile
      .find do
        'where':'id':params.id
        'attributes':visible-attributes
      .success

    unless found-profile
      return send.not-found 'Profile does not exist!'

    send.json 'profile':found-profile

  router.put '/:id' ({account-id, username, is-admin, body, params:{id}}, send)!->
    profile-to-update <-! profile
      .find id
      .success

    unless profile-to-update
      return send.not-found 'Profile does not exist!'

    unless account-id is profile-to-update.owner or is-admin
      return send.unauthorized 'No permission to edit profile of this user!'

    updated-profile <-! profile-to-update
      .update-attributes body.profile
      .success

    send.json 'profile':updated-profile
    io.emit \dataUpdate 'profile':updated-profile, 'username':username

  router.get '/:id/image' ({params}, send)!->
    found-profile <-! profile
      .find do
        'where':'id':params.id
        'attributes':<[image mimetype]>
      .success

    unless found-profile
      return send.not-found 'Profile does not exist!'

    unless found-profile.image
      return send.not-found 'Profile has no image!'

    send.content-type found-profile.mimetype
    send.end found-profile.image, \binary

  router.put '/:id/image' ({account-id, is-admin, params, files}, send)!->
    found-profile <-! profile
      .find do
        'where':'id':params.id
        'attributes':<[id image mimetype owner]>
      .success

    unless found-profile
      return send.not-found 'Profile does not exist!'

    unless account-id is found-profile.owner or is-admin
      return send.unauthorized 'No permission to edit profile of this user!'

    found-profile.mimetype = files.image.mimetype
    found-profile.image = files.image.buffer

    <-! found-profile.save!success

    send.end!