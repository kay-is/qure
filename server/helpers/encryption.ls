require! bcrypt

get-hash = ->
  salt = bcrypt.gen-salt-sync!
  password = bcrypt.hash-sync it, salt
  {salt, password}

compare = (password, salt, hash-to-compare)->
  get-hash password, salt
  .hash is hash-to-compare

module.exports = {get-hash, compare}