require! {
  'jsonwebtoken':jwt
  '../../config':web:{token-header, secret}
  '../helpers/logger'
}

auth-log = logger \Auth \yellow

module.exports =
  auth-checker: (request, send, next)!->
    # no auth check for reading files
    return next! if (request.url.slice -8) is \download and request.method is \GET

    token = request.headers[token-header]

    return send.bad-request "Access token missing in header: " + token-header unless token

    error, {username, account-id, is-admin} <-! jwt.verify token, secret

    return send.bad-request error if error

    request.is-admin = is-admin
    request.username = username
    request.account-id = account-id

    next!

  error-handlers:(request, send, next)!->
    send.not-found = !->
      send
      .status 404
      .json 'msg':(it or '404 - Not found!')

    send.accepted = !->
      send
      .status 202
      .end!

    send.bad-request = (msg)!->
      send
      .status 400
      .json 'msg':msg

    send.unauthorized = (msg)!->
      send
      .status 401
      .json 'msg':msg

    send.handle-error = (msg,number=500)->
      !->
        send
        .status number
        .json 'msg':msg

    next!