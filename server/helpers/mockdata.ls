require! {
  crypto
  yamljs:yaml
  './logger'
}
default-data-file-path = __dirname + '/../../database/default.yml'

log = logger \Default-Data \red

module.exports = (models, next)!->
  log \Initialize...

  default-data-file-path |> yaml.load |> format-data |> create-records

  function format-data(default-data)
    log 'Formatting default data!'
    records = []
    for model, record-definitions of default-data
      model-name = model.slice 0, -1
      for record-definition in record-definitions
        records.push 'model':model-name, 'record':record-definition
    log 'Creating records!'
    records

  !function create-records(default-data)
    record = default-data.shift!
    models[record.model]
    .create record.record
    .success !->
      log 'Created ' + record.model + ' record!'
      if default-data.length > 0
        create-records default-data
      else
        log \Initialized!
        next!
    .error !->
      log 'Error:' it
