require! chalk:c
require! moment

module.exports = (name, color)->
  !->
    time = moment!format 'HH:mm:ss.SSS'
    input = Array.prototype.slice.call arguments, 0
    input.unshift (c.white '[' + time + ']-[' + (c[color].bold name) + ']')
    console.log.apply console, input