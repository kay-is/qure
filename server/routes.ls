require! {
  fs
  express
  '../config':config
  './helpers/middleware':{auth-checker}
  './helpers/logger'
}
log = logger \Router \cyan

load-controllers = ->
  controllers = {}
  controller-dir = __dirname + '/controllers'
  controller-file-names = fs.readdirSync controller-dir

  for controller-file-name in controller-file-names
    controller-name = controller-file-name.slice 0, -3
    controllers[controller-name] = require './controllers/' + controller-name
  return controllers

module.exports = (app, models)->
  log 'Initialize...'
  controllers = load-controllers!

  for controller-name, controller of controllers
    router = express.Router!
    router.use auth-checker unless controller-name is \auth or controller-name is \images
    controller router, models, app.io
    app.use config.web.api-base-url + controller-name, router

  log 'Controllers:' (Object.keys controllers).join ', '

  app.use config.web.base-url + 'static/', express.static \static
  app.use config.web.base-url, express.static config.web.client-dir
  log 'Initialized!'