require! {
  fs
  sequelize
  './models/associations':associate
  '../config':{db}
  './helpers/logger'
  './helpers/mockdata'
}
log = logger \Sequelize \blue

module.exports = (next)!->
  log 'Initialize...'
  database = new sequelize do
    db.database,
    db.username,
    db.password,
    db.options

  log 'Initializing models...'

  models = {}

  model-definitions = fs.readdir-sync __dirname + '/models'

  for model-file-name in model-definitions
    continue if model-file-name is \associations.ls
    model-name = model-file-name.slice 0, -3
    model-definition = require __dirname + '/models/' + model-name
    models[model-name] = database.define model-name, model-definition

  associate models

  <-! database
    .sync force:true
    .success

  log 'Models:' (Object.keys models ).join ', '

  log 'Initializing mock-up data...'
  <-! mockdata models

  log 'Initialized!'
  next models