require! {
  http
  express
  multer
  \body-parser
  './helpers/logger'
  './database':init-database
  './socketio'
  './helpers/middleware':{error-handlers}
  './routes'
  '../config':{web}
}
log = logger \Server \magenta
one-mega-byte = 1000000bytes
module.exports = start:!->
  log 'Initialize...'
  app = express!
  web-server = http.Server app
  app.io = socketio web-server

  app.use body-parser.json limit:\50mb
  app.use multer do
    'inMemory':yes
    'limits':
      'files':1
      'fileSize':one-mega-byte
  app.use error-handlers

  models <-! init-database

  routes app, models

  <-! web-server.listen web.port, web.host
  log 'Web-server listening on http://' + web.host + ':' + web.port
  log 'Initialized!'