App.CreateroomController = Ember.Controller.extend do

  title: void
  description: void
  parent: void

  reset:!->
    @set \title void
    @set \description void
    @set \parentId void

  actions:
    create:!->
      parent-room = @get \model

      input = @get-properties <[title description]>

      new-room = @store.create-record \room input

      new-room.set \parent parent-room

      <~! new-room.save!then

      # TODO: Calling reload fixes broken relationships of the new room
      #       but also sends a new request to the server, so it needs a better solution
      new-room.reload!

      @transition-to-route \manageroom new-room

    cancel:!->
      @reset!
      @transition-to-route \manageroom (@get \model)