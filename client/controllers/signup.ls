App.SignupController = Ember.Controller.extend do

  needs:<[auth]>

  loading:no

  reset:!->
    @set-properties do
      'username':''
      'email':''
      'password':''
      'errorMessage':''

  show-error:({msg})!->
    @set \loading no
    @set \errorMessage msg

  hide-error:!-> @set \errorMessage void

  actions:
    signup:!->
      @set \loading yes
      @hide-error!

      auth-ctrl = @get \controllers.auth

      input = @get-properties \username \email \password

      send-json CONST.SIGNUP_URL, input
      .done ({token, username, account-id, is-admin})!~>

        <~! auth-ctrl.connect-socket token

        auth-ctrl.set \token token
        auth-ctrl.set \username username
        auth-ctrl.set \accountId account-id
        auth-ctrl.set \isAdmin is-admin

        @set \loading no
        @transition-to-route \homepage

      .fail extract-json @~show-error