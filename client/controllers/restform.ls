App.RestFormController = Ember.ObjectController.extend do

  go-back:!->
    route = @get \targetRoute
    @transition-to-route route, @get \model

  actions:
    save:!->
      record = @get \model

      <~! record.save!then

      @go-back!

    cancel:!->
      @get \model .rollback!

      @go-back!