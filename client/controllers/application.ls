App.ApplicationController = Ember.Controller.extend do

  needs:<[auth]>

  sockets:
    data-update:!->
      username = it.username
      delete it.username

      models = []
      for model, data of it
        @store.update model, data
        models.push model

      show-info 'User <strong>' + username + '</strong> updated a ' + models.join ', '

    data-delete:!->
      username = it.username
      delete it.username

      models = []
      for model, id of it
        record = @store.all model, id
        @store.unload-record record

      show-info 'User <strong>' + username + '</strong> deleted a ' + models.join ', '

  init:!->
    @_super ...

    if local-storage.token
      @get \controllers.auth
      .connect-socket local-storage.token
      @store.push \account JSON.parse local-storage['account']

    @init-nav-hiding!

  init-nav-hiding:!->
    last-position = 0

    $ window .scroll !->
      current-position = ($ @).scroll-top!
      down-scroll = last-position < current-position
      last-position := current-position

      if down-scroll
        $ \#room-toolbar .slide-up 60ms
      else
        $ \#room-toolbar .slide-down 60ms

  actions:
    logout:!->
      @get \controllers.auth .logout!