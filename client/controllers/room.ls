App.RoomController = Ember.ObjectController.extend do

  'needs': <[page auth]>

  'userKey': property \keys.@each ->
    room-id = @get \id
    @store
    .all \account
    .get \firstObject
    .get \keys
    .find (key)-> (key.get \room .get \id) is room-id

  # these are used to show/hide bottom menu buttons
  'filesModifiable': property \userKey -> @get \userKey .get \filesModifiable
  'filesReadable'  : property \userKey -> @get \userKey .get \filesReadable
  'pagesModifiable': property \userKey -> @get \userKey .get \pagesModifiable
  'pagesReadable'  : property \userKey -> @get \userKey .get \pagesReadable
  'roomModifiable' : property \userKey -> @get \userKey .get \roomModifiable

  'actions':
    'createPage':!->
      room = @get \model
      new-page = @store.create-record \page,
        'title':'New Page'
        'text':'**Edit this page!**'
        'room':room
      new-page.save!
      @transition-to-route \page room, new-page