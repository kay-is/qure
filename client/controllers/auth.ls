App.AuthController = Ember.Controller.extend do

  loading:no

  token: local-storage.token
  token-changed: observes \token !->
    token = @get \token
    if token
      local-storage.token = token
      $.ajax-setup 'headers':'x-access-token':local-storage.token
    else
      local-storage.remove-item \token
      $.ajax-setup 'headers':void

  reset:!->
    @set-properties do
      'username':''
      'password':''
      'errorMessage':''

  show-error:({msg})!->
    @set \loading no
    @set \errorMessage msg

  hide-error:!->
    @set \errorMessage void

  logout:!->
    @set \token void
    local-storage.remove-item \account

    @socket.socket.io.disconnect!

    window.location.hash = '/landingpage'
    window.location.reload!

  connect-socket:(token, callback)!->
    unless @socket.socket
      @socket.connect 'query':'token=' + token
    else
      @socket.socket.io.reconnect!

    @socket.socket.once \connect !~>
      callback! if callback

    @socket.socket.once \error !->
      console.error '[Socket.IO] Error: ' arguments

  actions:
    login:!->
      @set \loading yes
      @hide-error!

      input = @get-properties \username \password

      send-json CONST.TOKEN_URL, input
      .done (response)!~>

        <~! @connect-socket response.token

        @set \token response.token
        local-storage['account'] = JSON.stringify response.account
        @store.push \account response.account

        @set \loading no
        @transition-to-route \homepage

      .fail extract-json @~show-error