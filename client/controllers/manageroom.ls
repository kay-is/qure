App.ManageroomController = Ember.ObjectController.extend do

  accounts:void

  actions:
    save:!->
      room = @get \model

      if room.get \isDirty
        <~! room
          .save!
          .then

        activity \room \edit room.get \id

      @transition-to-route \room room

    delete:!->
      room = @get \model
      parent = room.get \parent

      room.delete-record!

      <~! room.save!then

      @transition-to-route \room parent

    create-key:!->
      @store
      .create-record \key 'room':@get \model
      .save!

    delete-key:(key-id)!->
      key <-! @store
        .find \key key-id
        .then
      key.delete-record!
      key.save!

    toggle-key-right:(key-id, right)!->
      key <-! @store
        .find \key key-id
        .then
      key
      .toggle right
      .save!