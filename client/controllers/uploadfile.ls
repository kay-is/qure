App.UploadfileController = Ember.Controller.extend do

  'fileData': void
  'filePath': void
  'fileName': void
  'mimeType': void

  file-path-change: observes \filePath !->
    file-path = @get \filePath

    if file-path
      file-name = file-path
        .split '\\'
        .pop!
      @set \fileName file-name

  reset:!->
    @set \fileData void
    @set \filePath void
    @set \fileName void

  go-back:!->
    @reset!
    room = @get \model
    page = room.get \firstPage
    @transition-to-route \page room, page

  actions:
    upload:!->
      room = @get \model
      file-data = @get \fileData
      file-name = @get \fileName
      mime-type = @get \mimeType

      new-file = @store.create-record \file do
        'name':file-name
        'content':file-data
        'room':room
        'mimeType':mime-type

      new-file.save!

      @go-back!

    cancel:!-> @go-back!