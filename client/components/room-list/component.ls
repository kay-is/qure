App.RoomListComponent = Ember.Component.extend do
  class-names: <[panel panel-primary room-list]>
  title:'Most Active Rooms'
  init:!->
    @_super!
    unless @store.rooms-loaded
      @set \rooms @store.find \room
      @store.rooms-loaded = yes
    else
      @set \rooms @store.all \room