App.BottomToolbarComponent = Ember.Component.extend do
  tag-name: \nav
  class-names: <[navbar navbar-default navbar-fixed-bottom hidden-md hidden-lg]>
  aria-role: \toolbar