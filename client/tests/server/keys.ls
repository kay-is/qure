describe '[Server] Keys Controller' !->
  url = api-url + '/keys'

  describe 'with token' !->
    headers = 'x-access-token':access-token

    _it "should create a new key" (done)!->
      key-data =
        'roomId':1
        'roomAccess':\read
        'pageAccess':\modify
        'fileAccess':\modify

      response <-! $
        .ajax do
          'type':\POST
          'url':url
          'data':key-data
          'headers':headers
        .done

      response.should.have.property \key
      response.key.should.have.property \pageAccess .equal \modify

      done!

    _it "should update a key" (done)!->
      key-data =
        'fileAccess':\prohibited

      update <-! $
        .ajax do
          'type':\PUT
          'url':url + '/1'
          'data':key-data
          'headers':headers
        .done

      update.should.have.property \key
      update.key.should.have.property \fileAccess .equal \prohibited

      done!

    _it "should deliver all keys of a user" (done)!->
      response <-! $
        .ajax do
          'type':\GET
          'url':url
          'data':'ownerId':1
          'headers':headers
        .done

      response.should.have.property \keys

      for key in response.keys
        key.should.have.property \ownerId .equal 1

      done!

    _it "should deliver all keys of a room" (done)!->
      response <-! $
        .ajax do
          'type':\GET
          'url':url
          'data':'roomId':1
          'headers':headers
        .done

      response.should.have.property \keys

      for key in response.keys
        key.should.have.property \roomId .equal 1

      done!

    _it "should deliver one key" (done)!->
      response <-! $
        .ajax do
          'type':\GET
          'url':url
          'data':
            'roomId':1
            'ownerId':1
          'headers':headers
        .done

      response.should.have.property \key

      response.key.should.have.property \roomId .equal 1
      response.key.should.have.property \ownerId .equal 1

      done!

  describe 'without token' !->
    _it "should NOT create a new key" (done)!->
      key-data =
        'roomId':1
        'roomAccess':\read
        'pageAccess':\modify
        'fileAccess':\modify

      {responseJSON} <-! $
        .ajax do
          'type':\POST
          'url':url
          'data':key-data
        .fail

      responseJSON.should.have.property \msg

      done!

    _it "should NOT update a key" (done)!->
      key-data =
        'fileAccess':\prohibited

      {responseJSON} <-! $
        .ajax do
          'type':\PUT
          'url':url + '/1'
          'data':key-data
        .fail

      responseJSON.should.have.property \msg

      done!

    _it "should NOT deliver all keys of a user" (done)!->
      {responseJSON} <-! $
        .ajax do
          'type':\GET
          'url':url
          'data':'ownerId':1
        .fail

      responseJSON.should.have.property \msg

      done!

    _it "should NOT deliver all keys of a room" (done)!->
      {responseJSON} <-! $
        .ajax do
          'type':\GET
          'url':url
          'data':'roomId':1
        .fail

      responseJSON.should.have.property \msg

      done!

    _it "should NOT deliver one key" (done)!->
      {responseJSON} <-! $
        .ajax do
          'type':\GET
          'url':url
          'data':
            'roomId':1
            'ownerId':1
        .fail

      responseJSON.should.have.property \msg

      done!
