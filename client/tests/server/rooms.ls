describe '[Server] Rooms Controller' !->
  url = api-url + '/rooms'

  describe 'with token' !->
    headers = 'x-access-token':access-token

    _it "should create a new room" (done)!->
      room-data =
        'title':'A New Test Room!'
        'description':'Wit a test description...'
        'parentId':1

      response <-! $
        .ajax do
          'type':\POST
          'url':url
          'data':room-data
          'headers':headers
        .done

      response.should.have.property \room

      done!