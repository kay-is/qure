describe '[Server] Auth Controller' !->
  var url
  var user-data

  before !->
    url := api-url + '/auth'
    user-data :=
      'username':'test' + Date.now!
      'password':'test' + Date.now!
      'email':'test@example.com' + Date.now!

  _it "should create a new user" (done)!->
    response <-! post-json url + '/signup', user-data

    response.should.have.property \token

    done!

  _it "should create a token" (done)!->
    response <-! post-json url + '/token', user-data

    response.should.have.property \token

    done!