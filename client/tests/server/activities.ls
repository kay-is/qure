describe '[Server] Activities Controller' !->
  url = api-url + '/activities'

  describe 'with token' !->

    headers = 'x-access-token':access-token
    _it "should create a new activity" (done)!->
      body =
        'name':'Test Activity'
        'url':'/tests'

      response <-! post-json url, body, headers
      .done

      response.should.have.property \activity

      response.activity
      .should.have.property \url
      .with.length 6

      done!

    _it "should deliver all activities of a user" (done)!->
      response <-! $
      .ajax do
        'type':\GET
        'url':url
        'headers':headers
      .done

      response.should.have.property \activities

      response.activities
      .should.have.property \length

      done!


  describe 'without token' !->

    _it "should NOT create a new activity" (done)!->
      body =
        'name':'Test Activity'
        'url':'/tests'

      {responseJSON} <-! post-json url, body
      .fail

      responseJSON.should.have.property \msg

      done!

    _it "should NOT deliver all activities of a user" (done)!->
      {responseJSON} <-! $
      .ajax do
        'type':\GET
        'url':url
      .fail

      responseJSON.should.have.property \msg

      done!