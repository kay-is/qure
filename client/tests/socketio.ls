describe 'Socket.IO' !->

  describe 'with token' !->
    var socket
    _it "should connect to server" (done)!->
      socket := io.connect 'query':'token=' + access-token
      <-! socket.once \connect

      socket.once \disconnect !-> done!

      socket.io.disconnect!

    _it "should reconnect to server" (done)!->
      socket.once \connect !->
        socket.once \disconnect !-> done!
        socket.io.disconnect!
      socket.io.reconnect!