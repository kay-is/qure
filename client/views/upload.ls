App.UploadView = Ember.TextField.extend do

  tag-name:\input
  class-names:<[form-control]>
  attribute-bindings:<[file]>
  type:\file

  file:void
  mime-type:void

  change:(e)!->
    reader = new FileReader!

    reader.onload = (e)!~>
      data-url-split = e.target.result.split ','
      mime-type = data-url-split.0.match /[.\w-/]+(?=;)/ .0
      file-data = data-url-split.1
      @set \file file-data
      @set \mimeType mime-type

    reader.read-as-data-URL e.target.files.0