Ember.Handlebars.helper \asciidoc (text='')->
  new Handlebars.SafeString Opal.Asciidoctor.$render text

Ember.Handlebars.helper \safe ->
  new Handlebars.SafeString it

Ember.Handlebars.helper \dateformat ->
  moment it
  .format 'D MMM YYYY (HH:mm:ss)'