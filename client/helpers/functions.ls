window.set-title = !->
  $ document .attr \title 'Qure - ' + it

window.send-json = (url, data)!->
  return $
  .ajax do
    'type':\POST
    'data':JSON.stringify data
    'contentType':'application/json'
    'url':url

window.show-info = (info-text)!->
  info-box = $ \#information

  info-box-content = $ '#information p'
  info-box-content.html info-text

  info-box.slide-down!

  set-timeout !->
    info-box.slide-up !->
      info-box-content.html ''
  , 3000ms

window.observes = (property, func)->
  func.observes property

window.property = (properties, func)->
  func.property properties

window.volatile = (func)->
  func.volatile!

window.extract-json = (func)->
  (response)!->
    func JSON.parse response.response-text

window.connect-socket-io = !->
  App.io = io.connect '' query:'token=' + local-storage.token

  disconnect = App.io~disconnect
  App.io.disconnect = !->
    disconnect!
    App = io:connect:connect-socket-io

  App.io.on \connect it