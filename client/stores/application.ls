App.ApplicationStore = DS.Store.extend do

  'adapter': App.ApplicationAdapter

  'aadidSaveRecord':(record, data)->
    unless data
      return @_super ...

    duplicate-record = @get-by-id record.constructor, data.id

    if duplicate-record
      duplicate-record.send \becameInvalid
      duplicate-record.unload-record!

    @_super ...

App.initializer do
  'name': \injectStoreIntoComponents
  'before': \registerComponentLookup
  'initialize': (container, application)!->
    container.injection \component \store \store:main