App.Router.map !->
  @route \landingpage
  @route \signup
  @route \auth 'path':'/login'

  @resource \homepage 'path':'/home'

  @resource \users
  @resource \profile 'path':'/profile/:profile_id'
  @resource \editprofile 'path':'/profile/:profile_id/edit'

  @resource \rooms
  @resource \createroom 'path':'/room/:parent_id/create'
  @resource \createkeys 'path':'/room/:room_id/keys/create'
  @resource \manageroom 'path':'/room/:room_id/manage'

  @resource \createpage 'path':'/room/:room_id/page/create'
  @resource \editpage 'path':'/room/:room_id/page/:page_id/edit'

  @resource \uploadfile 'path':'/room/:room_id/file/upload'

  @resource \room 'path':'/room/:room_id', !->

    @resource \pages
    @resource \page 'path':'/page/:page_id'

    @resource \files
    @resource \file 'path':'/file/:file_id'

    @resource \createkey 'path':'/key/create'