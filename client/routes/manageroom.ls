App.ManageroomRoute = Ember.Route.extend do

  model:({room_id})->
    @store.find \key 'room':room_id
    @store.find \room room_id

  setup-controller:(controller)!->
    @_super ...
    controller.set \accounts @store.find \account