App.RoomlistRoute = Ember.Route.extend do

  model:-> @store.find \room

  render-template:!->
    @render 'roomlist' do
      'into':'homepage'
      'outlet':'roomlist'