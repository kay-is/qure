App.RoomRoute = Ember.Route.extend do

  model:({room_id})->
    @store.find \room 'parent':room_id
    @store.find \page 'room':room_id
    @store.find \room room_id