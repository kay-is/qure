App.Account = DS.Model.extend do

  'username': DS.attr \string
  'isAdmin': DS.attr \boolean

  'keys':DS.has-many \key 'async':yes 'inverse':'owner'