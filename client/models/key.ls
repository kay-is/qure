App.Key = DS.Model.extend do

  'fileAccess': DS.attr \string 'defaultValue':\read
  'pageAccess': DS.attr \string 'defaultValue':\read
  'roomAccess': DS.attr \string 'defaultValue':\read

  'room': DS.belongs-to \room 'async':yes
  'owner': DS.belongs-to \account 'async':yes

  'toggle':(right)->
    switch @get right
    | \denied => @set right, \read
    | \read => @set right, \modify
    | _ => @set right, \denied
    return this

  'access':(access, right)-> (@get access) is right

  'fileAccessDenied': property \fileAccess -> @access \fileAccess \denied
  'fileAccessRead'  : property \fileAccess -> @access \fileAccess \read
  'fileAccessModify': property \fileAccess -> @access \fileAccess \modify

  'pageAccessDenied': property \pageAccess -> @access \pageAccess \denied
  'pageAccessRead'  : property \pageAccess -> @access \pageAccess \read
  'pageAccessModify': property \pageAccess -> @access \pageAccess \modify

  'roomAccessDenied': property \roomAccess -> @access \roomAccess \denied
  'roomAccessRead'  : property \roomAccess -> @access \roomAccess \read
  'roomAccessModify': property \roomAccess -> @access \roomAccess \modify

  'filesModifiable': property \fileAccess -> @access \fileAccess \modify
  'filesReadable':   property \fileAccess ->
    permission = @get \fileAccess
    permission is \read or permission is \modify

  'pagesModifiable': property \pageAccess -> @access \pageAccess \modify
  'pagesReadable':   property \pageAccess ->
    permission = @get \pageAccess
    permission is \read or permission is \modify

  'roomModifiable': property \roomAccess -> @access \roomAccess \modify
  'roomReadable':   property \roomAccess ->
    permission = @get \roomAccess
    permission is \read or permission is \modify