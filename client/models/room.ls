App.Room = DS.Model.extend do

  'title': DS.attr \string
  'description': DS.attr \string

  'firstPage': DS.belongs-to \page 'async':yes
  'parent': DS.belongs-to \room 'async':yes 'inverse':\rooms

  'files': DS.has-many \file 'asyc':yes
  'keys': DS.has-many \key 'async':yes
  'pages': DS.has-many \page 'async':yes
  'rooms': DS.has-many \room 'async':yes 'inverse':\parent