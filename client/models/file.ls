App.File = DS.Model.extend do
  'name': DS.attr \string
  'content': DS.attr \file
  'mimeType': DS.attr \string

  'url': property \id -> CONST.API_URL + 'files/' + (@get \id) + '/download'

  'room': DS.belongs-to \room 'async':yes