App.Profile = DS.Model.extend do
  'fullName':DS.attr \string
  'email':DS.attr \string
  'image':DS.attr \file
  'website':DS.attr \string
  'description':DS.attr \string
  'createdAt': DS.attr \date