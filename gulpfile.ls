require! {
  gulp
  path
  'gulp-concat':merge
  'gulp-cssshrink':cssshrink
  'gulp-ember-emblem':emblem
  'gulp-declare':declare
  'gulp-livescript':live-script
  'gulp-rimraf':remove
  'gulp-stylus':stylus
  'gulp-uglify':uglify
  'gulp-watch':watch
  'gulp-wrap':wrap
  'rimraf':remove-dir
  './server/index':server
}

task = gulp~task
source = gulp~src
destination = gulp~dest

client-dir = path.join __dirname, \client
bower-components-dir = __dirname + '/bower_components'
client-lib-dir = client-dir + '/libraries'
client-ls-file-pattern = client-dir + '/**/**.ls'
template-dir = path.join client-dir, \templates
client-em-file-pattern = template-dir + '/**/*.emblem'
client-em-component-file-pattern = client-dir + '/components/**/**.emblem'
client-styl-app-file = client-dir + '/styles/stylus/app.styl'

build-dir = path.join __dirname, \build

namespace-with-prefix =
  'namespace':\Ember.TEMPLATES
  'noRedeclare':true
  'processName': (component-path)->
    # the name of the last directory is the component name
    component-name = (component-path.split path.sep)[* - 2]
    return 'components/' + component-name

client-lib-js-files =
  * 'd3/d3.js'
  * 'asciidoctor.js/dist/asciidoctor-all.js'
  * 'marked/lib/marked.js'
  * 'socket.io-client/socket.io.js'
  * 'jquery/jquery.js'
  * 'handlebars/handlebars.js'
  * 'ember/ember.js'
  * 'ember-data/ember-data.js'
  * 'ember-sockets/dist/ember-sockets.js'
  * 'mocha/mocha.js'
  * 'chai/chai.js'
  * 'moment/moment.js'
for lib, i in client-lib-js-files
  client-lib-js-files[i] = path.join bower-components-dir, lib

client-lib-js-files.push path.join client-lib-dir, 'highlight/highlight.pack.js'

client-lib-css-files =
  * 'fontawesome/css/font-awesome.css'
  * 'bootswatch/cosmo/bootstrap.css'
  * 'mocha/mocha.css'
for lib, i in client-lib-css-files
  client-lib-css-files[i] = path.join bower-components-dir, lib

client-lib-js-files.push path.join client-lib-dir, 'highlight/styles/arta.css'

client-lib-font-files =
  * 'fontawesome/fonts/fontawesome-webfont.woff'
  * 'fontawesome/fonts/fontawesome-webfont.ttf'
for lib, i in client-lib-font-files
  client-lib-font-files[i] = path.join bower-components-dir, lib

client-index-html-file = path.join client-dir, 'index.html'
client-test-index-html-file = path.join client-dir, 'tests/index.html'

task \build-dev ->
  <-! remove-dir build-dir

  source client-index-html-file
  .pipe destination -> build-dir

  source client-test-index-html-file
  .pipe destination -> path.join build-dir, \tests

  source client-ls-file-pattern
  .pipe live-script!
  .pipe destination -> build-dir

  source client-em-file-pattern
  .pipe emblem!
  .pipe wrap 'Ember.Handlebars.template(<%= contents %>)'
  .pipe declare do
    'namespace':\Ember.TEMPLATES
    'noredeclare':true
    'processName':->
      it.replace template-dir + path.sep, ''
      .replace '.js', ''
      .replace '\\', '/'
  .pipe merge \templates.js
  .pipe destination -> build-dir

  source client-em-component-file-pattern
  .pipe emblem!
  .pipe wrap 'Ember.Handlebars.template(<%= contents %>)'
  .pipe declare namespace-with-prefix
  .pipe merge \components.js
  .pipe destination -> build-dir

  source client-styl-app-file
  .pipe stylus!
  .pipe merge \application.css
  .pipe destination -> build-dir

  source client-lib-js-files
  .pipe destination -> path.join build-dir, \libraries

  source client-lib-css-files
  .pipe destination -> path.join build-dir, \libraries

  source client-lib-font-files
  .pipe destination -> path.join build-dir, \fonts

task \server <[build-dev]> !-> server.start!

task \default <[server]>