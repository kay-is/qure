## Todo

- Login: **OK**
- Logout: **OK**
- Signup: **Incomplete**

## Needed Resources

- Homepage
  - Activities
    - Last 10 fullissued
  - Rooms
    - Last 10 active room IDs and Titles (for links)

- Room
  - Rooms
    - current room full
  - Pages
    - First page full
    - Page IDs and titles for links (last version!)
  - Rooms
    - Child room IDs of current room
  - Files
    - File IDs and names for links (last version!)

- Manage Room
  - 